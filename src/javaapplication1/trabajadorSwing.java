/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;


import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.DefaultListModel;
import javax.swing.SwingWorker;

/**
 *
 * @author nambr
 */
public class trabajadorSwing extends SwingWorker<Integer,String> {
    
    
    DefaultListModel listModel;
    int inicio;
    int fin;
    String[] ipInicio;
    String ipServer;
    String user;
    String pass;
    String rutaAgente;
    String rutaPsexec;
    
    @Override
    protected Integer doInBackground() throws Exception {
       int var = 1;
       publish("### Iniciando despliegue de agente ###");
       setProgress(0);
   
       for (int a = inicio; a<=fin; a++  ){
           
           String ip1 =ipInicio[0] + "." + ipInicio[1] + "." +ipInicio[2] +"."+ a ;
           
           String opcionesAgente = "/NOSPLASH /S /NOW /FORCE /SERVER=http://"+ipServer+"/ocsinventory";
      String total = rutaPsexec + " \\\\"+ip1+" -u "+user+" -p "+pass+" -c "+rutaAgente+" "+opcionesAgente;
      
     publish("Tratando con dirección IP: "+ip1);
     
     
     
   
     
     Process p;
     
         p = Runtime.getRuntime().exec(total);
	    
	int valorEspera = p.waitFor();
        setProgress(100);
       switch (valorEspera) {
              case 0:
                  publish("¡El agente se instaló correctamente!");
                  break;
              case 6:
                  publish("Error! Computadora apagada, o la dirección IP no está asignada a ninguna PC");
                  setProgress(100);
                  break;
              case 1326:
                  publish("Error! Las credenciales NO son correctas");
                  break;
              default:
                  publish("Ocurrió un error en el despliegue e instalación del agente");
                  
                  break;
          }
     
     
       }
       
       
       
       return var;
    }
    
   
    @Override
  protected void process(List<String> aMostrar) {
    for (String cadena : aMostrar) {
      listModel.addElement(cadena);
        
    }
  }
    
    trabajadorSwing(DefaultListModel lm, int inicio, int fin, String[] ipIni, String ipServ, String usu, String pass, String rutaPse, String rutaAge){
        this.listModel = lm;
        this.inicio = inicio;
        this.fin = fin;
        this.ipInicio = ipIni;
        this.ipServer = ipServ;
        this.user = usu;
        this.pass = pass;
        this.rutaAgente = rutaAge;
        this.rutaPsexec = rutaPse;
    }
    
}
