/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.SwingWorker;

/**
 *
 * @author nambr
 */
public class trabajadorSolitario extends SwingWorker<Integer,String> {

    
    DefaultListModel listModel;
    String comandoAEjecutar;
    String ip;
    
    @Override
    protected Integer doInBackground() throws Exception {
        int valor = 1;
        Process p;
     publish("Probando con IP: " +ip);
         p = Runtime.getRuntime().exec(comandoAEjecutar);
	    
	int valorEspera = p.waitFor();
     
       switch (valorEspera) {
              case 0:
                  publish(ip+": ¡El agente se instaló correctamente!");
                  break;
              case 6:
                  publish(ip+": Error! Computadora apagada, o la dirección IP no está asignada a ninguna PC");
                  break;
              case 1326:
                  publish(ip+": Error! Las credenciales NO son correctas");
                  break;
              default:
                  publish(ip+": Ocurrió un error en el despliegue e instalación del agente");
                  
                  break;
          }
        
        return valor;
         
    }
    
    @Override
      protected void process(List<String> aMostrar) {
    for (String cadena : aMostrar) {
      listModel.addElement(cadena);
        
    }
  }
      
      trabajadorSolitario(DefaultListModel dm, String comando, String ipA){
          
          this.listModel = dm;
          this.comandoAEjecutar = comando;
          this.ip = ipA;
          
      }
    
}
