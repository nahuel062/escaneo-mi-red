/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.concurrent.Callable;

/**
 *
 * @author nambr
 */
public class hiloDespliegue implements Callable<Integer> {

    
    String comandoAEjecutar;
    
    @Override
    public Integer call() throws Exception {
     Process p;
     
         p = Runtime.getRuntime().exec(comandoAEjecutar);
	    
	int valorEspera = p.waitFor();
        return valorEspera;
        
    }
    
    
    hiloDespliegue(String comand){
        this.comandoAEjecutar = comand;
    }
    
}
